package Browser_Lanuch;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Browser_lanuch {
	public static WebDriver driver;
	public static ExtentHtmlReporter htmlReports;      
    public static   ExtentReports extent;    
    public static ExtentTest Test;
    public static   String filename= "ExtentReportResults.html";
	@BeforeTest				
	public void Browser_Config() throws Exception {	    ///Test Ashok	  Commited date 11-02-2020
		try{
		    htmlReports = new ExtentHtmlReporter(filename);
	    	extent =new ExtentReports();
	    	extent.attachReporter(htmlReports);
	    	htmlReports.config().setReportName("Automation Testing");
	    	htmlReports.config().setTheme(Theme.STANDARD);
	    	htmlReports.config().setTestViewChartLocation(ChartLocation.TOP);
	    	extent.setSystemInfo("Project Name", "Maven Project Test");
	    	extent.setSystemInfo("User Name", "Ashok.A");
	    	extent.setSystemInfo("Environment", "Automation Testing");

	// System.setProperty("webdriver.chrome.driver","E:\\Automation\\Elipse_Workspace\\Driver\\chromedriver.exe");
	System.setProperty("webdriver.chrome.driver","E:\\Automation\\Elipse_Workspace\\Driver\\Chrome\\Chrome_79\\chromedriver.exe");
		 
 	    driver = new ChromeDriver();
 		driver.get("http://geo4s.logicvalley.co.uk:83/Geo4sUAT/Login/Login");	
 		driver.manage().window().maximize();
 		 Test =extent.createTest("TC-1:Browser open");
 		 Test.log(Status.PASS, "Browser launched");
		 } catch(Exception  e) { 
			     Test.log(Status.FAIL, "Test Case Failed is "+e);
			   throw(e);
	        }
	}	
}
